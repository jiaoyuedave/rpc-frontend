﻿#include "frontend.h"

#if !defined(__linux__)
#define FuncExport extern "C" __declspec(dllexport) // WIN32 DLL exporter
#else
#define FuncExport extern "C" // LINUX shared object exporter
#endif                        // !defined(__linux__)

#ifdef PLUGIN

FuncExport int parse(int argc, const char** argv) {
  Frontend f;
  return f.startup(argc, argv);
}

FuncExport int parse_string(const char* command_line) {
  Frontend f;
  return f.startup(command_line);
}

#else

int main(int argc, const char** argv) {
    Frontend f;
    return f.startup(argc, argv);
}

#endif // PLUGIN
